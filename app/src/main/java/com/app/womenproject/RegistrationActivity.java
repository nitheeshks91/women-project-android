package com.app.womenproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.womenproject.api.ServiceHandler;
import com.app.womenproject.api.model.ApiResponse;
import com.app.womenproject.fragment.MessageDialogFragment;
import com.app.womenproject.helper.Keys;
import com.app.womenproject.helper.PrefernceHelper;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


/**
 * Created by nitheesh on 27/3/18.
 */

public class RegistrationActivity extends AppCompatActivity {

    EditText usernameET;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        usernameET = findViewById(R.id.username);
        Button register = findViewById(R.id.register);
        showProgress(false);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(usernameET.getText())) {
                    createUserInDb(usernameET.getText().toString());
                }
            }
        });

    }

    private void createUserInDb(String username) {
        showProgress(true);
        ServiceHandler.getInstance()
                .createUser(username)
                .subscribe(new Observer<ApiResponse<String>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ApiResponse<String> res) {
                        showProgress(false);
                        if (res.status == 200) {
                            PrefernceHelper.saveBoolean(RegistrationActivity.this, Keys.USER_REGISTERED, true);
                            PrefernceHelper.saveString(RegistrationActivity.this, Keys.USERNAME, usernameET.getText().toString());

                            MessageDialogFragment messageDialogFragment = new MessageDialogFragment();
                            messageDialogFragment.setCancelable(false);
                            Bundle bundle = new Bundle();
                            bundle.putString("message", "Registration Successful.");
                            messageDialogFragment.setArguments(bundle);
                            messageDialogFragment.setListener(new MessageDialogFragment.OkCallBack() {
                                @Override
                                public void onOkClick() {
                                    startActivity(new Intent(RegistrationActivity.this, MapActivity.class));
                                    finish();
                                }
                            });
                            messageDialogFragment.show(getSupportFragmentManager(), "");
                        } else {
                            usernameET.setText("");
                            usernameET.setError(res.data);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        showProgress(false);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    void showProgress(boolean show) {
        findViewById(R.id.progressBg).setVisibility(show ? View.VISIBLE : View.GONE);
        findViewById(R.id.progressCard).setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
