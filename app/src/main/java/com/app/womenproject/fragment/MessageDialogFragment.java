package com.app.womenproject.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.womenproject.R;

/**
 * Created by nitheesh on 28/3/18.
 */

public class MessageDialogFragment extends AppCompatDialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.message_dialog, container, false);
        TextView textView = view.findViewById(R.id.message);
        String message = getArguments().getString("message", "");
        textView.setText(message);
        Button button = view.findViewById(R.id.ok);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) listener.onOkClick();
                dismiss();
            }
        });

        return view;
    }

    private OkCallBack listener;

    public void setListener(OkCallBack listener) {
        this.listener = listener;
    }

    public interface OkCallBack {
        void onOkClick();
    }
}
