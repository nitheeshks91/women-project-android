package com.app.womenproject.api.model;

/**
 * Created by nitheesh on 27/3/18.
 */

public class ApiResponse<T> {
    public int status;
    public T data;
}
