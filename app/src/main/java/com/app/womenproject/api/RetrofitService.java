package com.app.womenproject.api;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nitheesh on 27/3/18.
 */

public class RetrofitService {


    private static RetrofitService RETROFIT_SERVICE;

    public static RetrofitService getInstance() {
        if (RETROFIT_SERVICE == null) {
            return RETROFIT_SERVICE = new RetrofitService();
        }
        return RETROFIT_SERVICE;
    }

    public RetrofitApi getApi() {
        //Creating a retrofit object

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
// add your other interceptors …

// add logging as last interceptor
        httpClient.addInterceptor(logging);  // <-- this is the important line!

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RetrofitApi.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .client(httpClient.build())
                .build();

        //creating the api interface
        return retrofit.create(RetrofitApi.class);
    }
}
