package com.app.womenproject.helper;

/**
 * Created by nitheesh on 2/4/18.
 */

import java.util.List;

public class LegsObject {
    public Distance distance;
    public Duration duration;

    private List<StepsObject> steps;

    public LegsObject(List<StepsObject> steps) {
        this.steps = steps;
    }

    public List<StepsObject> getSteps() {
        return steps;
    }

    public class Distance {
        public String text;
    }

    public class Duration {
        public String text;
    }

}



