package com.app.womenproject.helper;

import rx.Observer;

/**
 * Created by nitheesh on 28/3/18.
 */

public class SimpleObserver<T> implements Observer<T> {
    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(T o) {

    }
}
