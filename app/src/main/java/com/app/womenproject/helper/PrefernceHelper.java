package com.app.womenproject.helper;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by nitheesh on 27/3/18.
 */

public class PrefernceHelper {

    private static SharedPreferences getPreference(Activity context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static boolean getBooolean(Activity context, String key) {
        return getPreference(context).getBoolean(key, false);
    }

    public static String getString(Activity context, String key) {
        return getPreference(context).getString(key, "");
    }


    public static void saveBoolean(Activity context, String key, boolean value) {
        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void saveString(Activity context, String key, String value) {
        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putString(key, value);
        editor.apply();
    }
}
