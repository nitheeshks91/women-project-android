package com.app.womenproject.helper;

/**
 * Created by nitheesh on 27/3/18.
 */

public class Keys {
    public static final String USER_REGISTERED = "user-registered";
    public static final String USERNAME = "username";

    public static final String FCM_UPDATED = "fcm-updated";
}
